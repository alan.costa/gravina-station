Channel setup

All microphones are connected to its own channel when loading the kit in DrumGizmo. 13 channels total. Remember to pan the relevant channels to give you a better stereo effect.

    Ch 1: Ambience left
    Ch 2: Ambience right
    Ch 3: Kickdrum back
    Ch 4: Kickdrum front
    Ch 5: Hihat
    Ch 6: Overhead left
    Ch 7: Overhead right
    Ch 8: Ride cymbal
    Ch 9: Snaredrum bottom
    Ch 10: Snaredrum top
    Ch 11: Tom1
    Ch 12: Tom2 (Floor tom)
    Ch 13: Tom3 (Floor tom)

