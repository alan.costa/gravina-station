# gravina station

Esse script instala alguns pacotes que eu uso no Debian para produzir música.

Boa parte dos softwares estão no repositório principal do Debian mas eu adicionei a opção de instalar o repositório do KXS Studio.

* Para instalar o repositório do KXS Studio e os plugins dele que eu uso no ardour é só adicionar o parâmetro 'kxs' quando for executar o gravina.sh.

Exemplo:

./gravina.sh kxs

Não se esqueça de dar permissão de execução ao script (chmod +x gravina.sh).

Os pacotes que estão no repositório do KXS Studio e constam nesse script possuem licenças livres (embora a licença do Luftikus não esteja tão clara), lá embaixo você pode conferir. 

Pacotes do repositório principal:

1 - qjackctl

2 - calf-plugins

3 - drumgizmo

4 - zynaddsubfx

5 - x42-plugins

6 - eq10q

7 - apt-transport-https


Pacotes do KXS Studio presentes no script:

1 - cadence (https://github.com/falkTX/Cadence/blob/master/COPYING)

2 - luftikus (https://www.audiopluginsforfree.com/luftikus/)


