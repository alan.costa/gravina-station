#!/usr/bin/env bash

clear
echo -e "\t\t	\t\t\tBEM VINDX À ESTAÇÃO GRAVINA\n"


#FUNÇÃO PARA INSTALAR OS PRINCIPAIS PACOTES

function audiosw {
    echo -e "Criando estrutura de pastas e arquivos do Drumgizmo\n"

    #Se não tiver a pasta do drumgizmo o script não será executado.

    [ ! -d drumgizmo ] && echo "Por favor, crie a estrutura de pastas do Drumgizmo." && exit 1

    for i in $(ls /home/); {
    mkdir -p /home/$i/ardour/drumgizmo-nuxdm4
    cp -rv drumgizmo/* /home/$i/ardour/drumgizmo-nuxdm4
    }
    echo "Instalando pacotes"
    apt-get -y install calf-plugins drumgizmo zynaddsubfx x42-plugins eq10q apt-transport-https  1>>logs/saida.log 2>>logs/erro.log

    echo "Pacotes instalados, em caso de problema, favor olhar a pasta 'logs'"

}

#FUNÇÃO PARA INSTALAR O REPO DO KXS STUDIO

function kxs {
  #Instalando wget porque a instação do repo do KXS Studio precisa baixar um .deb
  apt install wget 1>>logs/saida.log 2>>logs/erro.log

  verify_kx=$(dpkg -l | grep -o kxstudio-repos-gcc5)

  echo "Procurando repositório antigo do KX Studio"

  sleep 3

    if [[ $verify_kx == "kxstudio-repos-gcc5" ]];
    then

	echo "O repositório antigo foi encontrado"
	echo "Removendo repositório antigo"

        sleep 3
	
	dpkg --purge kxstudio-repos-gcc5 1>>logs/saida.log 2>>logs/erro.log

    else
	echo "Repositório antigo do KX Studio não foi encontrado"

    fi


    if [[ `dpkg -l | grep -o kxstudio-repos` ]];

    then

	echo "O repositório do kxstudio já está instalado"
	apt-get update 1>>logs/saida.log 2>>logs/erro.log
        echo "Instalando Cadence e Luftikus (pacotes do KX Studio)."
        apt-get -y install cadence luftikus 1>>logs/saida.log 2>>logs/erro.log

        sleep 3

    else

	   kx_url="https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos_10.0.3_all.deb"

	   if [[ `wget -S --spider $kx_url  2>&1 | grep 'HTTP/1.1 200 OK'` ]];

	   then
	       wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos_10.0.3_all.deb  1>>logs/saida.log 2>>logs/erro.log
	       echo "O .deb do KX Studio foi baixado"
	   else
	       echo "URL do KX Studio com defeito, favor verificar"
	   fi

	   dpkg -i kxstudio-repos_10.0.3_all.deb  1>>logs/saida.log 2>>logs/erro.log 
	   if [[ `dpkg -l | grep 'kxstudio-repos'` ]];

	   then
	       echo "Repositório do KX Studio instalado com sucesso"
               #atualizando repositorios para poder instalar os softwares contidos no kxstudio apos a instalacao do repo
               apt-get update 1>>logs/saida.log 2>>logs/erro.log
               echo "Instalando Cadence e Luftikus (pacotes do KX Studio)."
               apt-get -y install cadence luftikus 1>>logs/saida.log 2>>logs/erro.log
	   else
	       echo "Houve algum erro na instalação do repositório - Não foi encontrado no dpkg -l"
	   fi

	   rm kxstudio-repos_10.0.3_all.deb 1>>logs/saida.log 2>>logs/erro.log 
     fi	
}

#VERIFICA SE O USUÁRIO É ROOT, EM CASO POSITIVO EXECUTA AS FUNÇÕES

needroot=$(whoami)
if [[ $needroot == "root" ]];
then
    mkdir logs
    apt-get update 1>>logs/saida.log 2>>logs/erro.log 
    audiosw
    case $1 in
	kxs)
	    kxs
	    ;;
    esac
else
    echo "You need to be root."
fi

